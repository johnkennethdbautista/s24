let getCube = prompt('Enter a number you want to get the cube of: ');
console.log(`The cube of ${getCube} is ${getCube = Math.pow(getCube, 3)}`);

let address = [258, "Washington Ave NW", "California", 90011];
let [houseNumber, street, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

let animal = {
	name: 'Lolong',
	specie: 'saltwater crocodile',
	weight: '1075 kgs',
	measurement: '20 ft 3 in'
}
let {name,specie,weight,measurement} = animal;
console.log(`${name} was a ${specie}. He weighed at ${weight} with a measurement of ${measurement}.`);

let numbers = [1,2,3,4,5];
numbers.forEach(number =>{
	console.log(number);
})
let reduceNumber = numbers.reduce((x,y) => x + y);
console.log(reduceNumber);
class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();
myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);